export const state = () => ({
  bpList: [
    { title: 'xl', minWidth: '1280px' },
    { title: 'lg', minWidth: '1024px' },
    { title: 'md', minWidth: '768px' },
    { title: 'sm', minWidth: '0' },
  ],
})
